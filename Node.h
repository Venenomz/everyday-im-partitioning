#pragma once
#include "AABB.h"
#include "Tile.h"
#include "TiledWorldGenerator.h"
#include <vector>


class Node
{
public:
	AABBf m_boundingBox;

	Node* m_Parent;
	std::vector<Node*> m_childNodes;
	std::vector<Tile*> m_nodeContents;

	int m_currentLvlInTree = -1;

	int m_minSplitSize = 3;
	int m_minObjSplitSize = 5;

	Node()
	{
		m_Parent = nullptr;
	}

	Node(Node* _parent)
	{
		m_Parent = _parent;
	}

	~Node()
	{
		for (Node* nodePtr : m_childNodes)
			delete nodePtr;

		m_childNodes.clear();

		for (Tile* tilePtr : m_nodeContents)
			delete tilePtr;

		m_nodeContents.clear();
	}

	void AddObject(Tile* _tile);

	std::vector<Tile*> FindDesiredTiles(Vector2f _targetTile);

	bool SplitCheck();
};

