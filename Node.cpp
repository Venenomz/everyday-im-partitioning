#include "Node.h"

	void Node::AddObject(Tile* _tile)
	{
		// check if we already contain children
		if (m_childNodes.size() > 0)
		{
			// loop over all children
			for (auto child : m_childNodes)
			{
				if (child->m_boundingBox.Intersects(_tile->m_bounds))
					child->AddObject(_tile);
			}
		}
		// we have no children
		else
		{
			// add the tile
			m_nodeContents.push_back(_tile);

			// check if need to split
			if (SplitCheck())
			{
				// split
				auto min = Vector2f::Zero;
				auto max = Vector2f::Zero;

				// create the children
				auto topLeftChild = new Node();
				
				// set min/max values
				min = Vector2f(m_boundingBox.boxMin.X, m_boundingBox.Centre().Y);
				max = Vector2f(m_boundingBox.Centre().X, m_boundingBox.boxMax.Y);

				// Set bounding box and current level in tree for topLeftChild
				topLeftChild->m_boundingBox = AABBf(min, max);
				topLeftChild->m_currentLvlInTree = m_currentLvlInTree + 1;
				
				// Add the top left node to children
				m_childNodes.push_back(topLeftChild);

				// Repeat for all other nodes
#pragma region RepeatNodeSetUp

				// topRightChild Node
				auto topRightChild = new Node();
				
				// set min/max values
				min = m_boundingBox.Centre();
				max = m_boundingBox.boxMax;
				
				// Set bounding box and current level in tree for topRightChild
				topRightChild->m_boundingBox = AABBf(min, max);
				topRightChild->m_currentLvlInTree = m_currentLvlInTree + 1;
				
				// Add the top right node to children
				m_childNodes.push_back(topRightChild);

				// bottomLeftChild Node
				auto bottomLeftChild = new Node();
				
				// set min/max values
				min = m_boundingBox.boxMin;
				max = m_boundingBox.Centre();

				// Set bounding box and current level in tree for bottomLeftChild
				bottomLeftChild->m_boundingBox = AABBf(min, max);
				bottomLeftChild->m_currentLvlInTree = m_currentLvlInTree + 1;

				// Add the bottomLeftNode to children
				m_childNodes.push_back(bottomLeftChild);

				// bottomRightChild node
				auto bottomRightChild = new Node();
				
				// set min/max values
				min = Vector2f(m_boundingBox.Centre().X, m_boundingBox.boxMin.Y);
				max = Vector2f(m_boundingBox.boxMax.X, m_boundingBox.Centre().Y);

				// Set bounding box and current level in tree for bottomRightChild
				bottomRightChild->m_boundingBox = AABBf(min, max);
				bottomRightChild->m_currentLvlInTree = m_currentLvlInTree + 1;

				// Add the bottomRightNode to children
				m_childNodes.push_back(bottomRightChild);

#pragma endregion
				// loop over children
				for (auto child : m_childNodes)
				{
					// loop over all the objects
					for (auto _myTile : m_nodeContents) 
					{
						// if they overlap then add them to the child
						if (child->m_boundingBox.Intersects(_myTile->m_bounds))
							child->AddObject(_myTile);
					}
				}
			}
		}
	}

	std::vector<Tile*> Node::FindDesiredTiles(Vector2f _targetTile)
	{
		if (m_childNodes.size() > 0)
		{
			for (auto child : m_childNodes)
			{
				if (child->m_boundingBox.Contains(_targetTile))
					return child->FindDesiredTiles(_targetTile);
			}
		}

		return m_nodeContents;
	}

	bool Node::SplitCheck()
	{
		if (m_nodeContents.empty())
			return false;

		else if (m_nodeContents.size() >= m_minObjSplitSize && (m_boundingBox.Width() >= m_minObjSplitSize && m_boundingBox.Height() >= m_minObjSplitSize))
			return true;

		return false;
	}